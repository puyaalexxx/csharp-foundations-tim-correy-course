﻿using System;

namespace less1_variables
{
    class Program
    {
        static void Main(string[] args)
        {
            string firstName = "test";
            string path = @"C:\\ddd\ss";

            //concatenation 
            string fullName = firstName + " suki";

            //string interpolation
            string fullname2 = $" {firstName} dddd";

            int age = 0;

            double d = 34.4;
            decimal dec = 33.555M;

            bool isAlive = false;

  
            Console.WriteLine(firstName);
        }
    }
}
