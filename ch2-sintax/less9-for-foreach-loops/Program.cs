﻿using System;

namespace less9_for_foreach_loops
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }

            string[] firstnames = new string[] { "Tim", "Bob", "John" };

            foreach (var name in firstnames)
            {
                Console.WriteLine(name);
            }
        }
    }
}
