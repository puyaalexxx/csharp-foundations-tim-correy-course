﻿using System;

namespace less2_conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            IfMethod();
        }

        static void IfMethod()
        {
            string firstName = "";

            Console.Write("First name: ");
            firstName = Console.ReadLine();

            if (firstName.ToLower() == "tim")
            {
                Console.WriteLine("Welcome Mr Correy");
            }
            else
            {
                Console.WriteLine($"Hello {firstName}");
            }
        }

        static void SwitchMethod()
        {
            string firstName = "";

            Console.Write("First name: ");
            firstName = Console.ReadLine();

            switch (firstName.ToLower())
            {
                case "tim":
                    Console.WriteLine("Welcome Mr Correy");
                    break;
                default:
                    Console.WriteLine($"Hello {firstName}");
                    break;
            }
        }
    }
}
