﻿using System;

namespace less10_methods
{
    class Program
    {
        static void Main(string[] args)
        {
            SayAuthor();
        }

        private static void SayAuthor()
        {
            Console.WriteLine("Method call");
        }
    }
}
