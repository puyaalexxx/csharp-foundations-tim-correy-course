﻿using System;

namespace less4_miniproject
{
    class Program
    {
        static void Main(string[] args)
        {
            string newname = "";

            Console.Write("Please let me know your name: ");
            string name = Console.ReadLine();

            Console.Write("Please let me know your age: ");
            bool isValidAge = int.TryParse(Console.ReadLine(), out int age);

            if (name.ToLower() == "bob" || name.ToLower() == "sue")
            {
                newname = $"Professor {name}";
            }
            else
            {
                newname = name;
            }

            if (isValidAge)
            {
                if (age < 21)
                {
                    Console.WriteLine($"I recommend to wait {21 - age} to start this class {newname}");
                }
                else
                {
                    Console.WriteLine($"Welcome to the class {newname}");
                }
            }
            else
            {
                Console.WriteLine("Your age is not valid");
            }
        }
    }
}
