﻿using System;
using System.Collections.Generic;

namespace less7_lists
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> firstNames = new List<string>()
            {
                "Alex", "Bob", "Andrew"
            };

            List<string> names = new List<string>();

            names.Add("Tim");
            names.Add("John");

            foreach (var name in firstNames)
            {
                Console.WriteLine(name);
            }
        }
    }
}
