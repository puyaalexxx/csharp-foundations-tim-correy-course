﻿using System;
using System.Collections.Generic;

namespace less11_miniproject
{
    class Program
    {
        private static List<string> parties = new List<string>();
        private static string moreGuestsComing = "";
        private static int totalGuests = 0;

        static void Main(string[] args)
        {
            AddParties();

            Console.WriteLine();
            PrintGuests();

            Console.WriteLine();
            PrinttotalGuest();
        }

        private static void AddParties()
        {
            do
            {
                string partyName = GetInfoFromConsole("What is the name of your party/group: ");

                parties.Add(partyName);

                totalGuests += GetPartySize();

                moreGuestsComing = GetInfoFromConsole("Do you want to add another guest (yes/no): ");

            } while (moreGuestsComing.ToLower() == "yes");
        }

        private static void PrintGuests()
        {
            Console.WriteLine("Guest Parties at Event:");

            foreach (var name in parties)
            {
                Console.WriteLine(name);
            }
        }

        private static void PrinttotalGuest()
        {
            Console.WriteLine("Total guests: {0}", totalGuests);
        }

        private static int GetPartySize()
        {
            bool isValidNumber = false;
            int partySize = 0;
            
            do
            {
                string partySizeText = GetInfoFromConsole("How many people are in your party? ");

                isValidNumber = int.TryParse(partySizeText, out partySize);

            } while (isValidNumber == false);

            return partySize;
        }

        private static string GetInfoFromConsole(string message)
        {
            Console.WriteLine(message);
            string output = Console.ReadLine();

            return output;
        }
    }
}
