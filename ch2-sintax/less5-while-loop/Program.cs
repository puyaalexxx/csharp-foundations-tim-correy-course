﻿using System;

namespace less5_while_loop
{
    class Program
    {
        static void Main(string[] args)
        {
            string continueResult = "";
            do
            {
                Console.WriteLine("Do you want to continue (yes/no): ");
                continueResult = Console.ReadLine();

            } while (continueResult.ToLower() == "yes");

            Console.WriteLine("Do you want to continue (yes/no): ");
            continueResult = Console.ReadLine(); 

            while (continueResult.ToLower() == "yes")
            {
                Console.WriteLine("something important");
            }
        }
    }
}
