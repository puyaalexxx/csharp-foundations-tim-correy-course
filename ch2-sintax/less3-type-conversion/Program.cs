﻿using System;

namespace less3_type_conversion
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is your age: ");
            bool isValidAge = int.TryParse(Console.ReadLine(), out int age);

            if (isValidAge)
            {
                Console.WriteLine("Welcome!");
            }
        }
    }
}
