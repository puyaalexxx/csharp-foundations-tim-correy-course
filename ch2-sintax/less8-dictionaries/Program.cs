﻿using System;
using System.Collections.Generic;

namespace less8_dictionaries
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> importantYears = new Dictionary<int, string>()
            {
                [1990] = "Alex",
                [1992] = "Andrew"
            };

            Dictionary<int, string> years = new Dictionary<int, string>();

            years[1989] = "Something happened";
            years[2001] = "Good year";

            foreach (var year in importantYears.Keys)
            {
                Console.WriteLine($"This is {year}");
            }
        }
    }
}
